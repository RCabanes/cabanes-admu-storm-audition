class PostsController < ApplicationController
  def index
    @posts = Post.all
  end

  def new
    @post = Post.new
  end
  
  def create
    @post = Post.new(post_params)
    
    if @post.save
      redirect_to posts_path
    else
      render :new
    end
  end
  
  def destroy
    @post = Post.find_by(id: params[:id])
    
    if @post.present?
      @post.destroy
    end
    
    redirect_to posts_path
  end
  
  def edit
    @post = Post.find_by(id: params[:id])
    
    if !@post.present?
      redirect_to posts_path
    end
  end
  
  def update
    @post = Post.find_by(id: params[:id])
    
    if @post.present?
      @post.update(post_params)
    end
    
    redirect_to posts_path
  end
  
  def show
    @post = Post.find_by(id: params[:id])
  end
  
  private
    def post_params
      params.require(:post).permit!
    end

end
